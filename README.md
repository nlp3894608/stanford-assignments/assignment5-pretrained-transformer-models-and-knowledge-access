<h1>CS 224N Assignment 5: Pretrained Transformer Models and Knowledge Access</h1>
<p> The assignment focuses on training Transformer models to perform tasks involving knowledge access, particularly in the context of question answering. Transformer model is train to attempt to answer simple questions of the form “Where was person [x] born?” – without providing any input text from which to draw the answer. This models are able to learn some facts
about where people were born through pretraining, and access that information during fine-tuning to
answer the questions.</p>
