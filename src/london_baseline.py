# Calculate the accuracy of a baseline that simply predicts "London" for every
#   example in the dev set.
# Hint: Make use of existing code.
# Your solution here should only be a few lines.
import utils

path = '../birth_dev.tsv'

with open(path, encoding='utf-8') as f:
    num_lines = len(f.readlines())

predictions = ["London"] * num_lines

total, correct = utils.evaluate_places(path, predictions)
if total > 0:
    print('Correct: {} out of {}: {}%'.format(correct, total, correct / total * 100))
